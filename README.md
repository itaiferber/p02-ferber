# 2048 Clone
This is a simplified, poor man's 2048 clone, as specified in the CS 441 [project requirements](http://cs.binghamton.edu/~pmadden/courses/cs580/p02-2048.html). It handles basic game mechanics, allowing a user to interact with the game via buttons or gestures. The game plays until there are no more moves possible, keeping track of the user's score.

This is not meant to be an example of _good_ game design, just _quick and simple_ game design.
