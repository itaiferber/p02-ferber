//
//  TFEGameViewController.m
//  p02-ferber
//
//  Created by Itai Ferber on 2/6/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import "TFEGameViewController.h"
@import AudioToolbox;

#pragma mark Private Interface
@interface TFEGameViewController ()

/*!
 A system sound ID corresponding to the sound played when the user swipes on
 the board and makes a valid move.
 */
@property (readwrite) SystemSoundID validSwipeSoundID;

/*!
 A system sound ID corresponding to the sound played when the user swipes on
 the board and makes an invalid move.
 */
@property (readwrite) SystemSoundID invalidSwipeSoundID;

@end

#pragma mark -
@implementation TFEGameViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"valid" withExtension:@"aif"];
    if (soundURL) {
        SystemSoundID soundID = 0;
        OSStatus status = AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &soundID);
        if (status != 0) {
            NSLog(@"Unable to register system sound.");
        } else {
            self.validSwipeSoundID = soundID;
        }
    } else {
        NSLog(@"Unable to load sound URL.");
    }

    soundURL = [[NSBundle mainBundle] URLForResource:@"invalid" withExtension:@"aif"];
    if (soundURL) {
        SystemSoundID soundID = 0;
        OSStatus status = AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &soundID);
        if (status != 0) {
            NSLog(@"Unable to register system sound.");
        } else {
            self.invalidSwipeSoundID = soundID;
        }
    } else {
        NSLog(@"Unable to load sound URL.");
    }

    self.game = [TFEGame new];

    // Ensure the delegate and dataSource properties are set, even if not hooked
    // up in IB.
    self.gameView.delegate = self.gameView.delegate ? : self;
    self.gameView.dataSource = self.gameView.dataSource ?: self;

    self.gameView.backgroundColor = [UIColor clearColor];
}

- (void)dealloc
{
    AudioServicesDisposeSystemSoundID(self.validSwipeSoundID);
    AudioServicesDisposeSystemSoundID(self.invalidSwipeSoundID);
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - User Interaction
- (void)swipe:(TFEDirection)direction
{
    BOOL validMove = [self.game performSwipeInDirection:direction];
    AudioServicesPlaySystemSound(validMove ? self.validSwipeSoundID : self.invalidSwipeSoundID);

    [self.gameView reloadData];
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %lu", (unsigned long)self.game.score];

    if (self.game.gameOver) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Game Over"
                                                                                 message:[NSString stringWithFormat:@"No more moves are possible. Your final score is %lu.", (unsigned long)self.game.score]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"New Game" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            self.game = [TFEGame new];
            self.scoreLabel.text = @"Score: 0";
            [self.gameView reloadData];
        }]];

        [self showViewController:alertController sender:self];
    }
}

- (IBAction)recognizeSwipe:(UISwipeGestureRecognizer *)sender
{
    switch (sender.direction) {
        case UISwipeGestureRecognizerDirectionUp:    [self swipe:TFEDirectionUp];    break;
        case UISwipeGestureRecognizerDirectionRight: [self swipe:TFEDirectionRight]; break;
        case UISwipeGestureRecognizerDirectionDown:  [self swipe:TFEDirectionDown];  break;
        case UISwipeGestureRecognizerDirectionLeft:  [self swipe:TFEDirectionLeft];  break;
    }
}

- (IBAction)swipeUp:(UIButton *)sender
{
    [self swipe:TFEDirectionUp];
}

- (IBAction)swipeRight:(UIButton *)sender
{
    [self swipe:TFEDirectionRight];
}

- (IBAction)swipeDown:(UIButton *)sender
{
    [self swipe:TFEDirectionDown];
}

- (IBAction)swipeLeft:(UIButton *)sender
{
    [self swipe:TFEDirectionLeft];
}

#pragma mark - UICollectionViewDataSource Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return collectionView == self.gameView && section == 0 ? 16 : -1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return collectionView == self.gameView ? 1 : -1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * const TFEGameViewCellIdentifier = @"TFEGameViewCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TFEGameViewCellIdentifier
                                                                           forIndexPath:indexPath];

    // Since the cell is being reused, we will need to "clear it" of any old
    // subviews it might still have.
    NSArray<UIView *> *subviews = cell.subviews;
    for (UIView *view in subviews) {
        [view removeFromSuperview];
    }

    cell.layer.backgroundColor = [UIColor colorWithRed:205.0/255.0
                                                 green:193.0/255.0
                                                  blue:180.0/255.0
                                                 alpha:1.0].CGColor;
    cell.layer.cornerRadius = 5.0;

    TFELocation location = {
        .x = indexPath.row % 4,
        .y = indexPath.row / 4
    };

    NSUInteger value = [self.game valueAtLocation:location];
    if (value > 0) {
        UILabel *label = [[UILabel alloc] initWithFrame:cell.bounds];
        label.font = [UIFont boldSystemFontOfSize:30.0];
        label.text = [NSString stringWithFormat:@"%lu", value];
        label.textColor = [UIColor colorWithRed:119.0/255.0
                                          green:110.0/255.0
                                           blue:101.0/255.0
                                          alpha:1.0];

        label.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:label];

        if (value == 2) {
            cell.layer.backgroundColor = [UIColor colorWithRed:238.0/255.0
                                                         green:228.0/255.0
                                                          blue:218.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 4) {
            cell.layer.backgroundColor = [UIColor colorWithRed:237.0/255.0
                                                         green:224.0/255.0
                                                          blue:200.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 8) {
            cell.layer.backgroundColor = [UIColor colorWithRed:242.0/255.0
                                                         green:177.0/255.0
                                                          blue:121.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 16) {
            cell.layer.backgroundColor = [UIColor colorWithRed:244.0/255.0
                                                         green:148.0/255.0
                                                          blue:99.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 32) {
            cell.layer.backgroundColor = [UIColor colorWithRed:246.0/255.0
                                                         green:124.0/255.0
                                                          blue:95.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 64) {
            cell.layer.backgroundColor = [UIColor colorWithRed:246.0/255.0
                                                         green:94.0/255.0
                                                          blue:59.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 128) {
            cell.layer.backgroundColor = [UIColor colorWithRed:237.0/255.0
                                                         green:207.0/255.0
                                                          blue:114.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 256) {
            cell.layer.backgroundColor = [UIColor colorWithRed:237.0/255.0
                                                         green:204.0/255.0
                                                          blue:97.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 512) {
            cell.layer.backgroundColor = [UIColor colorWithRed:238.0/255.0
                                                         green:199.0/255.0
                                                          blue:99.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 1024) {
            cell.layer.backgroundColor = [UIColor colorWithRed:237.0/255.0
                                                         green:197.0/255.0
                                                          blue:64.0/255.0
                                                         alpha:1.0].CGColor;
        } else if (value == 2048) {
            cell.layer.backgroundColor = [UIColor colorWithRed:244.0/255.0
                                                         green:148.0/255.0
                                                          blue:99.0/255.0
                                                         alpha:1.0].CGColor;
        } else {
            cell.layer.backgroundColor = [UIColor colorWithRed:60.0/255.0
                                                         green:48.0/255.0
                                                          blue:49.0/255.0
                                                         alpha:1.0].CGColor;
        }
    }

    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - UICollectionViewDelegate Methods
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Cells in the game view should not be highlightable.
    return collectionView == self.gameView ? NO : YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    // Cells in the game view should not be selectable.
    return collectionView == self.gameView ? NO : YES;
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat viewWidth = collectionView.bounds.size.width;
    CGFloat intercellSpacing = [self collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:indexPath.section];
    CGFloat cellWidth = (viewWidth - (intercellSpacing * 3)) / 4;
    return CGSizeMake(cellWidth, cellWidth);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return collectionView.bounds.size.width / (100.0/3);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    // Elements should have the same amount of spacing in between in both
    // directions.
    return [self collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:section];
}

@end
