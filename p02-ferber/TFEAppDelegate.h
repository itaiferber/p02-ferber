//
//  TFEAppDelegate.h
//  p02-ferber
//
//  Created by Itai Ferber on 2/6/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;

@interface TFEAppDelegate : UIResponder <UIApplicationDelegate>

#pragma mark - Properties
// The app's main window.
@property (strong, nonatomic) UIWindow *window;

@end
