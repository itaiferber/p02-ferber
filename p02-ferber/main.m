//
//  main.m
//  p02-ferber
//
//  Created by Itai Ferber on 2/6/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFEAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TFEAppDelegate class]));
    }
}
