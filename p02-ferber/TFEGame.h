//
//  TFEGame.h
//  p02-ferber
//
//  Created by Itai Ferber on 2/6/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import Foundation;

#pragma mark Type Definitions
//! A TFELocation is used to refer to locations on the 2048 game grid.
typedef struct {
    //! The x-coordinate of the location.
    unsigned char x;

    //! The y-coordinate of the location.
    unsigned char y;
} TFELocation;

//! A TFEDirection represents a direction the user can swipe in.
typedef enum {
    TFEDirectionUp,
    TFEDirectionRight,
    TFEDirectionDown,
    TFEDirectionLeft
} TFEDirection;

#pragma mark - TFEGame
/*!
 Represents an instance of a 2048 game, including the current state of the game
 grid, score, etc.
 */
@interface TFEGame : NSObject

#pragma mark - Properties
//! The board representation of the game. A linear array of grid values.
@property (readonly) NSArray<NSNumber *> *board;

//! The current total score.
@property (readonly) NSUInteger score;

//! Stores whether the game board has filled up and no more moves are possible.
@property (readonly, getter=isGameOver) BOOL gameOver;

#pragma mark - Initialization
// Initializes a new game object with two filled tiles.
- (instancetype)init;

#pragma mark - Accessors
/*!
 Returns the value at the board tile at the given location.
 
 \param location The location to check.
 \returns Zero if the grid spot is empty; a positive power of 2 otherwise.
 */
- (NSUInteger)valueAtLocation:(TFELocation)location;

#pragma mark - Accepting User Actions
/*!
 Performs a user swipe in the given direction, coalescing numbers and updating
 the game score.
 
 \param direction The direction the user swiped in.
 \returns Whether the given swipe resulted in a valid move.
 */
- (BOOL)performSwipeInDirection:(TFEDirection)direction;

@end
