//
//  TFEGame.m
//  p02-ferber
//
//  Created by Itai Ferber on 2/6/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import "TFEGame.h"

#pragma mark Private Interface
@interface TFEGame ()

#pragma mark - Private Properties
@property (readwrite) NSUInteger score;
@property (readwrite) BOOL gameOver;

@end

#pragma mark -
@implementation TFEGame

#pragma mark - Initialization
- (instancetype)init
{
    if (!(self = [super init])) {
        return nil;
    }

    NSMutableArray *array = [NSMutableArray array];
    for (size_t i = 0; i < 16; ++i) {
        [array addObject:@0];
    }

    // Lazy man's way of setting two unique random board spots to 2's.
    u_int32_t index = arc4random_uniform(16);
    array[index] = @2;

    u_int32_t otherIndex = arc4random_uniform(16);
    while (otherIndex == index) {
        otherIndex = arc4random_uniform(16);
    }

    array[otherIndex] = @2;

    _board = array;
    _score = 0;
    _gameOver = NO;
    return self;
}

#pragma mark - Accessors
- (NSUInteger)valueAtLocation:(TFELocation)location
{
    return [self.board[location.y * 4 + location.x] unsignedIntegerValue];
}

#pragma mark - Accepting User Actions
- (BOOL)performSwipeInDirection:(TFEDirection)direction
{
    // Easier to work with a C-style array for this.
    NSUInteger grid[4][4];
    BOOL merged[4][4];
    for (NSUInteger i = 0; i < 4; i += 1) {
        for (NSUInteger j = 0; j < 4; j += 1) {
            grid[i][j] = [self.board[i * 4 + j] unsignedIntegerValue];
            merged[i][j] = NO;
        }
    }

    BOOL boardHasChanged = NO;
    if (direction == TFEDirectionUp || direction == TFEDirectionDown) {
        NSInteger deltaY = direction == TFEDirectionUp ? 1 : -1;
        for (NSInteger column = 0; column < 4; column += 1) {
            NSInteger row = direction == TFEDirectionUp ? 1 : 2;
            NSInteger endRow = direction == TFEDirectionUp ? 4 : -1;
            for (; row != endRow; row += deltaY) {
                NSUInteger value = grid[row][column];
                if (value == 0) {
                    continue;
                }

                // Value needs to be moved down as far as possible, and possibly
                // merged with values below.
                NSUInteger endNext = 3 - endRow; // Swap between -1 and 4.
                for (NSUInteger next = row - deltaY; next != endNext; next -= deltaY) {
                    NSUInteger nextValue = grid[next][column];
                    if (nextValue == 0) {
                        // This spot is available to be moved into.
                        grid[next + deltaY][column] = 0;
                        grid[next][column] = value;
                        boardHasChanged = YES;
                    } else if (nextValue == value) {
                        // We might be able to merge the values together if the
                        // cell below doesn't yet represent a merged value.
                        if (!merged[next][column]) {
                            grid[next + deltaY][column] = 0;
                            grid[next][column] *= 2;
                            self.score += grid[next][column];
                            merged[next][column] = YES;
                            boardHasChanged = YES;
                        }

                        // If we merged, we're done. If we weren't able to
                        // merge, there's nothing left to do; we're done too.
                        break;
                    } else {
                        // We can't move further down, and we can't merge with
                        // the value.
                        break;
                    }
                }
            }
        }
    } else {
        NSInteger deltaX = direction == TFEDirectionLeft ? 1 : -1;
        for (NSInteger row = 0; row < 4; row += 1) {
            NSInteger column = direction == TFEDirectionLeft ? 1 : 2;
            NSInteger endColumn = direction == TFEDirectionLeft ? 4 : -1;
            for (; column != endColumn; column += deltaX) {
                NSUInteger value = grid[row][column];
                if (value == 0) {
                    continue;
                }

                // Value needs to be moved down as far as possible, and possibly
                // merged with values below.
                NSUInteger endNext = 3 - endColumn; // Swap between -1 and 4.
                for (NSUInteger next = column - deltaX; next != endNext; next -= deltaX) {
                    NSUInteger nextValue = grid[row][next];
                    if (nextValue == 0) {
                        // This spot is available to be moved into.
                        grid[row][next + deltaX] = 0;
                        grid[row][next] = value;
                        boardHasChanged = YES;
                    } else if (nextValue == value) {
                        // We might be able to merge the values together if the
                        // cell below doesn't yet represent a merged value.
                        if (!merged[row][next]) {
                            grid[row][next + deltaX] = 0;
                            grid[row][next] *= 2;
                            self.score += grid[row][next];
                            merged[row][next] = YES;
                            boardHasChanged = YES;
                        }

                        // If we merged, we're done. If we weren't able to
                        // merge, there's nothing left to do; we're done too.
                        break;
                    } else {
                        // We can't move further down, and we can't merge with
                        // the value.
                        break;
                    }
                }
            }
        }
    }

    // Check to see if there are any more empty locations on the board. If there
    // are and the move was valid, add a 2 to a random location on the board.
    // Otherwise, if there are no open spots, check to see if any moves are even
    // possible.
    NSMutableArray<NSValue *> *emptyLocations = [NSMutableArray array];
    for (NSUInteger i = 0; i < 4; i += 1) {
        for (NSUInteger j = 0; j < 4; j += 1) {
            if (grid[i][j] == 0) {
                TFELocation l = {.x = j, .y = i};
                NSValue *value = [NSValue valueWithBytes:&l
                                                objCType:@encode(typeof(l))];
                [emptyLocations addObject:value];
            }
        }
    }

    if (emptyLocations.count == 0) {
        // There are no more empty spaces on the board. Check to see if any
        // moves are still possible. If not, the game is over.
        BOOL movesStillPossible = NO;
        for (NSUInteger i = 0; i < 3; i += 1) {
            for (NSUInteger j = 0; j < 3; j += 1) {
                if ((i < 2 && grid[j][i] == grid[j][i + 1]) ||
                    (j < 2 && grid[j][i] == grid[j + 1][i])) {
                    movesStillPossible = YES;
                    break;
                }
            }

            if (movesStillPossible) {
                break;
            }
        }

        self.gameOver = !movesStillPossible;
    } else if (boardHasChanged) {
        // The move was valid, and there are available empty spots to insert a 2
        // into.
        NSUInteger index = arc4random_uniform((u_int32_t)emptyLocations.count);
        TFELocation l;
        [emptyLocations[index] getValue:&l];

        grid[l.y][l.x] = 2;
    }

    if (!boardHasChanged) {
        return NO;
    }

    NSMutableArray<NSNumber *> *board = (NSMutableArray<NSNumber *> *)self.board;
    for (NSUInteger i = 0; i < 4; i += 1) {
        for (NSUInteger j = 0; j < 4; j += 1) {
            board[i * 4 + j] = @(grid[i][j]);
        }
    }

    return YES;
}

@end
