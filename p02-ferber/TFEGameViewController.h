//
//  TFEGameViewController.h
//  p02-ferber
//
//  Created by Itai Ferber on 2/6/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;
#import "TFEGame.h"

// The application's main view controller.
@interface TFEGameViewController : UIViewController <UICollectionViewDataSource,
                                                     UICollectionViewDelegate,
                                                     UICollectionViewDelegateFlowLayout>

#pragma mark - Properties
//! The game currently being played through the controller.
@property TFEGame *game;

//! The collection view in which the game's cells will appear.
@property IBOutlet UICollectionView *gameView;

//! The label above the game view that shows the current score.
@property IBOutlet UILabel *scoreLabel;

#pragma mark - User Interaction
/*!
 A swipe action called from a gesture recognizer on the game view. Performs a
 swipe in the recognizer's direction and updates the game view.
 
 \param sender The gesture recognizer that sent the action.
 */
- (IBAction)recognizeSwipe:(UISwipeGestureRecognizer *)sender;

/*!
 A swipe action called from a button above the game view. Performs an upward
 swipe and updates the game view.

 \param sender The button that sent the action.
 */
- (IBAction)swipeUp:(UIButton *)sender;

/*!
 A swipe action called from a button above the game view. Performs a rightward
 swipe and updates the game view.

 \param sender The button that sent the action.
 */
- (IBAction)swipeRight:(UIButton *)sender;

/*!
 A swipe action called from a button above the game view. Performs a leftward
 swipe and updates the game view.

 \param sender The button that sent the action.
 */
- (IBAction)swipeDown:(UIButton *)sender;

/*!
 A swipe action called from a button above the game view. Performs a downward
 swipe and updates the game view.

 \param sender The button that sent the action.
 */
- (IBAction)swipeLeft:(UIButton *)sender;

@end
